VERSION=v0.1.0

documentation:
	schubert document
	VERSION=${VERSION} bash doc/generate.sh > documentation.md

test:
	guile --r7rs -L . test.scm
