(import (scheme base)
        (scheme write)
        (retropikzel time v0.1.0 main))


(time-timeout
  (lambda ()
    (display "Hello from timeout")
    (newline))
  3000)

(time-interval
  (lambda ()
    (display "Hello from interval")
    (newline))
  1000)

(define main
  (lambda ()

    (time-update)))

(main)
