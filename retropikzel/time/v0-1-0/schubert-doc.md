### time-as-seconds

Get the current timestamp as seconds


### time-as-millisecons

Get the current timestamp as milliseconds


### time-update

Update the internal timer and run any timeouts or intervals

You need to call this frequently to make the timeouts and intervals work


### time-timeout

Arguments:

- proc (procedure)
  - Procedure you want to be run after given time
- milliseconds (number)
  - Number of milliseconds after you want your procedure to be run

Returns (number) Index of the timeout


### time-interval

Arguments:

- proc (procedure)
  - Procedure you want to be run on the interval
- milliseconds (number)
  - Length of the interval

Returns (number) Index of the interval


### time-stop

Arguments:

- index (number)
  - Index of the timeout or interval returned by either time-timeout or time-interval


