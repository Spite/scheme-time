Utility library for time related stuff for R7RS Schemes.

For documentation see
[Wiki](https://codeberg.org/Spite/scheme-time/wiki/Documentation)

For bugs you can use the
[Bugs](https://codeberg.org/Spite/scheme-time/projects/9099)
